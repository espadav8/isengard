#!/usr/bin/env sh
set -e

docker info

docker build \
    -t "${CONTAINER_IMAGE}" \
    -f .docker/Dockerfile \
    .

docker push "${CONTAINER_IMAGE}"
