#!/usr/bin/env sh
set -e

docker pull "${CONTAINER_IMAGE}"

docker tag "${CONTAINER_IMAGE}" "${LATEST_CONTAINER_IMAGE}"

docker push "${LATEST_CONTAINER_IMAGE}"
