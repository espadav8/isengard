import graphene
import sys
import os

from gino import Gino
from graphql.execution.executors.asyncio import AsyncioExecutor
from starlette.applications import Starlette
from starlette.graphql import GraphQLApp

from isengard.schemas import schema


def get_database_connection_string():
    DATABASE_HOST = os.environ.get('DATABASE_HOST', 'postgres')
    DATABASE_NAME = os.environ.get('DATABASE_NAME', 'isengard')
    DATABASE_PASSWORD = os.environ.get('DATABASE_PASSWORD', '')
    DATABASE_USERNAME = os.environ.get('DATABASE_USERNAME', 'isengard')

    CREDENTIALS = ''

    if DATABASE_USERNAME != '':
        CREDENTIALS = '{}:{}@'.format(DATABASE_USERNAME, DATABASE_PASSWORD)

    return "postgresql://{}{}/{}".format(CREDENTIALS, DATABASE_HOST, DATABASE_NAME)

DATABASE_URL = get_database_connection_string()

db = Gino()

app = Starlette()
app.add_route(
    '/',
    GraphQLApp(
        schema=schema.schema,
        executor_class=AsyncioExecutor
    )
)

@app.on_event('startup')
async def startup():
    await db.set_bind(DATABASE_URL)

@app.on_event("shutdown")
async def shutdown():
    await db.pop_bind().close()
